package helpers

import (
	"os"
	"os/signal"
)

func OnSignalHandler(sig os.Signal, fn func()) {
	c := make(chan os.Signal, 1)
	signal.Notify(c, sig)
	go func() {
		for _ = range c {
			fn()
		}
	}()
}

func Contains(s []int, e int) bool {
    for _, a := range s {
        if a == e {
            return true
        }
    }
    return false
}
