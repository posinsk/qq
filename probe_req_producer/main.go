package main

import (
	"encoding/json"
	"log"
	"qq/cli"
	"qq/common"
	"qq/models"
	"time"

	"gopkg.in/mgo.v2/bson"
	"strconv"
)

func main() {

	log.Printf("Connecting to Rabbitmq...")

	producerMap := map[int]common.Producer{}

	for interval, _ := range models.CHECK_INTERVALS {
		queueName := common.QUEUE_NAME_PROBE_REQUEST + "_" + strconv.Itoa(interval)
		producerMap[interval] = common.NewProducer(*cli.OptionRabbitUri, queueName)
	}

	log.Printf(" [*] Waiting for probes. To exit press CTRL+C")

	for {

		checks := models.FindCheckForProbe()

		if len(checks) == 0 {
			log.Printf("Waiting for new active probes...")
			time.Sleep(time.Second * 2)
		} else {
			time.Sleep(time.Second * 2)

			ids := make([]bson.ObjectId, 0)

			for _, check := range checks {

				marshalledCheck, err := json.Marshal(common.NewProbeRequestMessage(&check))
				if err == nil {
					ids = append(ids, check.Id)
					prod := producerMap[check.CheckInterval]
					prod.Send(marshalledCheck)
				}
			}
			models.UnsetNextCheckTime(ids)
			log.Printf("Added %d probes to queue", len(checks))
		}
	}

}
