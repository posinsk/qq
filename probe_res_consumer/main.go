package main

import (
	"encoding/json"
	"log"
	"qq/common"
	"qq/cli"
	"github.com/streadway/amqp"
	"gopkg.in/mgo.v2/bson"
	"qq/models"
)

func main() {

	cons := common.NewConsumer(*cli.OptionRabbitUri, common.QUEUE_NAME_PROBE_RESPONSE)

	forever := make(chan bool)

	go func() {
		cons.Consume(func(d amqp.Delivery) {
			pr := common.ProbeResponseMessage{}
			err := json.Unmarshal(d.Body, &pr)

			if err != nil {
				return
			}

			probe := pr.ToProbe()
			chId := bson.ObjectIdHex(pr.CheckId)

			models.AddProbeToGroup(chId, probe)
			models.UpdateNextCheckTime(pr.CheckInterval, chId)

			log.Printf("Added probe to check %s", pr.CheckId)

			d.Ack(false)
		})
	}()

	log.Printf(" [*] Waiting for messages. To exit press CTRL+C")
	<-forever
}
