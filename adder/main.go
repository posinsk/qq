package main

import . "qq/common"

func main() {

	hosts := []string{
		"http://wp.pl", "http://onet.pl", "http://interia.pl",
		"http://o2.pl", "http://gazeta.pl", "http://tvn24.pl",
		"http://tvnwarszawa.pl", "http://facebook.com", "http://bip.msz.gov.pl",
		"http://www.bip.gov.pl", "http://tvnwarszawa.pl", "http://news.ycombinator.com",
		"http://youtube.com", "http://kwejk.pl", "http://google.pl",
		"http://bip.ksp.policja.gov.pl", "http://bip.piaseczno.policja.waw.pl",
		"http://bip.krp6.policja.waw.pl/", "http://dupa777.pl",
	}

	for _, address := range hosts {
		c, err := NewCheck(cg, address, address)
		if err != nil {
			panic(err)
		}

		c.Save(&DbInstance)
	}
}
