package main

import (
	"encoding/xml"
	"fmt"
	"gopkg.in/mgo.v2/bson"
	"io/ioutil"
	"os"
	"qq/models"
	"strings"
)

type BipCol struct {
	Key   string `xml:"name,attr"`
	Value string `xml:",chardata"`
}

type BipXmlRow struct {
	XMLName xml.Name `xml:"row"`
	Cols    []BipCol `xml:"col"`
}

type RecurlyEntries struct {
	XMLName     xml.Name    `xml:"bip"`
	Version     string      `xml:"version,attr"`
	Rows        []BipXmlRow `xml:"row"`
	Description string      `xml:",innerxml"`
}

func main() {
	file, err := os.Open("allsubjects.xml") // For read access.
	fmt.Println("Reading file...")
	if err != nil {
		fmt.Printf("error: %v", err)
		return
	}
	defer file.Close()
	fmt.Println("Reading bytes from file...")
	data, err := ioutil.ReadAll(file)
	if err != nil {
		fmt.Printf("error: %v", err)
		return
	}
	v := RecurlyEntries{}
	err = xml.Unmarshal(data, &v)
	if err != nil {
		fmt.Printf("error: %v", err)
		return
	}

	fmt.Println("Iterating...")
	for _, row := range v.Rows {
		var name string
		var val string
		for _, col := range row.Cols {
			if col.Key == "name" {
				name = col.Value
			}
			if col.Key == "www" {
				val = col.Value
			}
		}

		if len(name) == 0 || len(val) == 0 {
			continue
		}

		if strings.Contains(val, "http://") == false {
			val = "http://" + val
		}

		c := models.NewCheck(bson.ObjectIdHex("5611184529bebd2495000001"), name, val, models.CHECK_TYPE_HTTP_STATUS, 2000, 120)
		c.ResponseStatus = 200
		c.Save()

		fmt.Println(name, val)
	}
}
