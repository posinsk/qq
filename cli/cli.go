package cli

import "flag"

var (
	OptionRabbitUri           = flag.String("rabbit_uri", "amqp://guest:guest@127.0.0.1:5672/", "AMQP URI")
	OptionProbeReqWorkerNum   = flag.Int("probe_req_worker_num", 50, "Number of concurrent requests")
	OptionProbeReqWorkerQueue = flag.Int("probe_req_worker_queue", 15, "Name of worker queue")
	OptionMongoUri            = flag.String("mongo_uri", "127.0.0.1", "MONGO URI")
	OptionMongoDbName         = flag.String("mongo_db", "qq", "MONGO DB Name")
)

func init() {
	//	flag.Parse()
}
