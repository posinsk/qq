package models

import (
	"net/url"
	"qq/helpers"
	"time"

	"github.com/revel/revel"
	"gopkg.in/mgo.v2/bson"
)

const CHECK_TYPE_HTTP_STATUS = "HTTP_STATUS"
const CHECK_TYPE_HTTP_CONTAINS = "HTTP_CONTAINS"
const CHECK_TYPE_PING = "PING"

var CHECK_TYPES = map[int]string{
	1: CHECK_TYPE_HTTP_STATUS,
	2: CHECK_TYPE_HTTP_CONTAINS,
	3: CHECK_TYPE_PING,
}

func GetCheckTypeByInt(t int) string {
	var _v string
	for k, v := range CHECK_TYPES {
		if k == t {
			_v = v
			break
		}
	}
	return _v
}

func GetCheckTypeByString(t string) int {
	var _k int
	for k, v := range CHECK_TYPES {
		if v == t {
			_k = k
			break
		}
	}
	return _k
}

var CHECK_INTERVALS = map[int]bool{120: true, 300: true, 600: true, 900: true, 1800: true, 2700: true, 3600: true}

type Check struct {
	Id             bson.ObjectId `bson:"_id,omitempty"`
	UserId         bson.ObjectId `bson:"uid"`
	Name           string        `bson:"name"`
	Type           int           `bson:"type"`
	ResponseText   string        `bson:"res_text,omitempty"`
	ResponseStatus int           `bson:"res_status,omitempty"`
	Address        url.URL       `bson:"url"`
	TimeoutMs      int           `bson:"tms"`
	CheckInterval  int           `bson:"chi"` //w sekundach
	NextProbeTime  time.Time     `bson:"npt,omitempty"`
	LastProbeTime  time.Time     `bson:"lpt,omitempty"`
	CreatedAt      time.Time     `bson:"created_at"`
}

func NewCheck(
	uid bson.ObjectId,
	name, address, chType string,
	timeoutMs, checkInterval int,
) *Check {

	parsedUrl, _ := url.Parse(address)
	c := new(Check)
	c.Address = *parsedUrl
	c.Name = name
	c.NextProbeTime = time.Now()
	c.CheckInterval = checkInterval
	c.Type = GetCheckTypeByString(chType)
	c.TimeoutMs = timeoutMs
	c.Id = bson.NewObjectId()
	c.CreatedAt = time.Now()
	c.UserId = uid

	return c
}

func (c *Check) ValidateNewHttpStatus(v *revel.Validation) {
	v.Required(c.ResponseStatus).Message("Oczekiwany status jest wymagany")
}

func (c *Check) ValidateNewHttpContains(v *revel.Validation) {
	v.Required(c.ResponseText).Message("Oczekiwany tekst jest wymagany")
}

func (c *Check) ValidateNew(v *revel.Validation, user User) {
	v.Required(c.Address).Message("Adres jest wymagany")
	v.Required(c.Name).Message("Nazwa jest wymagana")
	v.Required(c.UserId).Message("UserId jest wymagany")
	v.Range(c.TimeoutMs, 20, 10000).Message("Timeout odpowiedzi powinien mieścić się w przedziale 20 a 10 000 ms")

	if _, ok := CHECK_INTERVALS[c.CheckInterval]; !ok {
		v.Error("Interwał sprawdzania jest nieprawidłowy").Key("chi")
	}

	if !user.isCheckIntervalAvailable(c.CheckInterval) {
		v.Error("Interwał sprawdzania jest niedostępny dla wybranego planu").Key("chi")
	}

	if GetCheckCount(user.Id) > user.SubscriptionPlan.MaxChecks {
		v.Error("Osiągnięto maksymalną ilość czujek")
	}

	if !helpers.Contains(user.SubscriptionPlan.AvailableCheckTypes, c.Type) {
		v.Error("Nie możesz dodać czujki tego typu")
	}

	if _, ok := CHECK_TYPES[c.Type]; !ok {
		v.Error("Nieznany typ czujki").Key("type")
	}

	if len(c.Address.Host) == 0 {
		v.Error("Adres musi byc URL-em").Key("address")
	}
}

func (c *Check) Save() error {
	err := DbInstance.getCollection(CHECK_COLLECTION_NAME).Insert(c)

	if err != nil {
		return err
	}
	return nil
}

func GetCheckCount(uid bson.ObjectId) int {
	var res int
	res, _ = DbInstance.getCollection(CHECK_COLLECTION_NAME).Find(bson.M{"uid": uid}).Count()
	return res
}

func FindOneCheck(id bson.ObjectId, uid bson.ObjectId) (Check, error) {
	var res Check
	err := DbInstance.getCollection(CHECK_COLLECTION_NAME).Find(bson.M{"_id": id, "uid": uid}).One(&res)
	return res, err
}

func FindAllChecks(uid bson.ObjectId) []Check {
	var res []Check

	q := bson.M{"uid": uid}

	DbInstance.getCollection(CHECK_COLLECTION_NAME).Find(q).All(&res)
	return res
}

func FindCheckForProbe() []Check {
	var res []Check
	q := bson.M{"npt": bson.M{"$lte": time.Now()}}
	DbInstance.getCollection(CHECK_COLLECTION_NAME).Find(q).Limit(100).All(&res)
	return res
}

func UnsetNextCheckTime(ids []bson.ObjectId) {
	selector := bson.M{"_id": bson.M{"$in": ids}}
	modifier := bson.M{"$unset": bson.M{"npt": true}}

	DbInstance.getCollection(CHECK_COLLECTION_NAME).UpdateAll(selector, modifier)
}

//func FindProbesForCheck(chid bson.ObjectId, from time.Time, to time.Time) []Probe{

//}

func UpdateNextCheckTime(interval int, checkId bson.ObjectId) {
	inter := time.Second * time.Duration(interval)

	modifier := bson.M{
		"$set": bson.M{"npt": time.Now().Add(inter), "lpt": time.Now()},
	}

	DbInstance.getCollection(CHECK_COLLECTION_NAME).UpdateId(checkId, modifier)
}
