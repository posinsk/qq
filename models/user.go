package models

import (
	"crypto/sha1"
	"encoding/hex"
	"gopkg.in/mgo.v2/bson"
	"qq/helpers"
	"time"
)
import "gopkg.in/mgo.v2"
import "github.com/revel/revel"

var SUB_PLAN_1 = SubscriptionPlan{
	Type:                1,
	AvailableIntervals:  []int{900, 1800, 2700, 3600},
	AvailableCheckTypes: []int{1, 3},
	MaxChecks:           3,
	MaxHistory:          7,
}

var SUB_PLAN_2 = SubscriptionPlan{
	Type:                2,
	AvailableIntervals:  []int{300, 600, 900, 1800, 2700, 3600},
	AvailableCheckTypes: []int{1, 2, 3},
	MaxChecks:           10,
	MaxHistory:          30,
}

var SUB_PLAN_3 = SubscriptionPlan{
	Type:                3,
	AvailableIntervals:  []int{120, 300, 600, 900, 1800, 2700, 3600},
	AvailableCheckTypes: []int{1, 2, 3},
	MaxChecks:           30,
	MaxHistory:          60,
}

type User struct {
	Id               bson.ObjectId    `bson:"_id,omitempty"`
	Email            string           `bson:"email"`
	Password         string           `bson:"password"`
	SubscriptionPlan SubscriptionPlan `bson:"plan"`
	CreatedAt        time.Time        `bson:"created_at"`
	LastLoginAt      time.Time        `bson:"last_login,omitempty"`
}

type SubscriptionPlan struct {
	Type                int   `bson:"type"`
	AvailableIntervals  []int `bson:"available_intervals"`
	AvailableCheckTypes []int `bson:"available_check_types"`
	MaxChecks           int   `bson:"max_checks"`
	MaxHistory          int   `bson:"max_history"` //w dniach
}

func NewUser(email string, password string) User {
	u := User{Id: bson.NewObjectId(), Email: email, Password: password}
	return u
}

func (user *User) SetSubscriptionPlan(plan int) {
	switch plan {
	case 1:
		user.SubscriptionPlan = SUB_PLAN_1
	case 2:
		user.SubscriptionPlan = SUB_PLAN_2
	case 3:
		user.SubscriptionPlan = SUB_PLAN_3
	}
}

func (user *User) ValidateRegister(v *revel.Validation) {
	v.Required(user.Email).Message("Email jest wymagany")
	v.Required(user.Password).Message("Haslo jest wymagane")
	v.Min(user.SubscriptionPlan.Type, 1).Message("Nie wybrano planu subskrypcji").Key("plan")

	if v.HasErrors() {
		return
	}

	v.Email(user.Email).Message("To nie jest adres email")
	v.MinSize(user.Password, 6).Message("Haslo powinno miec przynajmniej 6 znakow")

	if len(user.Email) > 0 {
		if _, err := FindUserByEmail(user.Email); err == nil {
			v.Error("Uzytkownik o takim adresie email juz istnieje").Key("email")
		}
	}

	if !v.HasErrors() {
		user.Password = hashPassword(user.Password)
		user.CreatedAt = time.Now()
	}
}

func (user *User) ValidateLogin(v *revel.Validation) *User {
	v.Required(user.Email).Message("Email jest wymagany")
	v.Email(user.Email).Message("To nie jest adres email")
	v.Required(user.Password).Message("Haslo jest wymagane")

	if len(user.Email) > 0 {
		uDb, err := FindUserByEmail(user.Email)
		if err != nil {
			v.Error("Uzytkownik o takim adresie nie istnieje").Key("email")
		}
		if !v.HasErrors() {
			passValid := uDb.ValidateUserPassword(user.Password)

			if !passValid {
				v.Error("Haslo niepoprawne").Key("password")
			}
		}
		return &uDb
	}

	return user
}

func (u *User) ValidateUserPassword(pass string) bool {
	return hashPassword(pass) == u.Password
}

func hashPassword(p string) string {
	data := []byte(p)
	hasher := sha1.New()
	hasher.Write(data)
	return hex.EncodeToString(hasher.Sum(nil))
}

func (u *User) isCheckIntervalAvailable(interval int) bool {
	return helpers.Contains(u.SubscriptionPlan.AvailableIntervals, interval)
}

func GetUserCollection() *mgo.Collection {
	return DbInstance.Database.C(USER_COLLECTION_NAME)
}

func FindUserByEmail(email string) (User, error) {
	u := User{}
	err := GetUserCollection().Find(bson.M{"email": email}).One(&u)
	return u, err
}

func FindUserById(id bson.ObjectId) (User, error) {
	u := User{}
	err := GetUserCollection().FindId(id).One(&u)
	return u, err
}

func UpdateLastLogin(uid bson.ObjectId) {
	GetUserCollection().UpdateId(uid, bson.M{"$set": bson.M{"last_login": time.Now()}})
}

func (u *User) Save() {
	GetUserCollection().Upsert(bson.M{"_id": u.Id}, u)
}
