package models

import (
	"log"
	"time"

	"strconv"

	"gopkg.in/mgo.v2/bson"
)

type ProbeGroup struct {
	TimestampPeriod int           `json:"tsp" bson:"tsp"`
	CheckId         bson.ObjectId `json:"cid" bson:"cid"`
	Probes          []Probe       `json:"probes" bson:"probes"`
}

type Probe struct {
	Timestamp time.Time `json:"ts" bson:"ts"`
	Timeout   bool      `json:"tm" bson:"tm"`
	Status    int       `json:"st" bson:"st,omitempty"`
	Duration  int64     `json:"dr" bson:"dr"`
	Valid     bool      `json:"v,omitempty" bson:"v,omitempty"`
}

func timeToTsp(t time.Time) int {
	tsp, _ := strconv.Atoi(t.Format("0612"))
	return tsp
}

func AddProbeToGroup(chid bson.ObjectId, probe Probe) {
	s := bson.M{"cid": chid}
	u := bson.M{
		"$push": bson.M{"probes": probe},
		"$set":  bson.M{"tsp": timeToTsp(probe.Timestamp), "cid": chid},
	}

	_, err := DbInstance.getCollection(PROBE_COLLECTION_NAME).Upsert(s, u)
	if err != nil {
		panic(err)
	}
}

func GetProbes(chid bson.ObjectId, from, to time.Time) []Probe {
	var res []ProbeGroup
	var res2 []Probe

	s := bson.M{"cid": chid, "tsp": bson.M{"$gte": timeToTsp(from), "$lte": timeToTsp(to)}}

	err := DbInstance.getCollection(PROBE_COLLECTION_NAME).Find(s).All(&res)

	if err != nil {
		panic(err)
	}

	log.Println(err, res, timeToTsp(from), timeToTsp(to))
	for _, probegroup := range res {
		log.Println(probegroup.Probes)
		res2 = append(res2, probegroup.Probes...)
	}
	return res2
}
