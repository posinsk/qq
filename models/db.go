package models

import "gopkg.in/mgo.v2"
import "qq/cli"

type DB struct {
	Database *mgo.Database
}

var DbInstance DB

const USER_COLLECTION_NAME = "user"
const CHECK_GROUP_COLLECTION_NAME = "check_group"
const CHECK_COLLECTION_NAME = "check"
const PROBE_COLLECTION_NAME = "probe"

func init() {

	DbInstance = *NewDBSession(*cli.OptionMongoUri, *cli.OptionMongoDbName)

	idx := mgo.Index{
		Key:        []string{"npt", "lpt"},
		Unique:     false,
		DropDups:   true,
		Background: true,
		Sparse:     true,
	}
	DbInstance.getCollection(CHECK_COLLECTION_NAME).EnsureIndex(idx)
}

func NewDBSession(host string, dbName string) *DB {
	session, err := mgo.Dial(host)

	if err != nil {
		panic(err)
	}

	db := new(DB)
	db.Database = session.DB(dbName)

	return db
}

func (db DB) getCollection(name string) *mgo.Collection {
	return db.Database.C(name)
}
