package common

import (
	"fmt"
	"github.com/streadway/amqp"
	"log"
)

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
		panic(fmt.Sprintf("%s: %s", msg, err))
	}
}

const QUEUE_NAME_PROBE_REQUEST = "probe_requests"
const QUEUE_NAME_PROBE_RESPONSE = "probe_responses"

type Queue struct {
	Channel    amqp.Channel
	Queue      amqp.Queue
	Connection amqp.Connection
}

func NewQueue(ch amqp.Channel, name string, conn amqp.Connection) *Queue {
	_q, err := ch.QueueDeclare(
		name,  // name
		true,  // durable
		false, // delete when unused
		false, // exclusive
		false, // no-wait
		nil,   // arguments
	)
	failOnError(err, "Failed to declare a queue")

	q := new(Queue)
	q.Queue = _q
	q.Channel = ch
	q.Connection = conn

	return q
}

type Producer Queue

func NewProducer(uri string, queueName string) Producer {
	conn, err := amqp.Dial(uri)
	failOnError(err, "Failed to connect to RabbitMQ")

	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")

	q := NewQueue(*ch, queueName, *conn)

	return Producer(*q)
}

func (p *Producer) Send(body []byte) {
	err := p.Channel.Publish(
		"",           // exchange
		p.Queue.Name, // routing key
		false,        // mandatory
		false,
		amqp.Publishing{
			DeliveryMode: amqp.Persistent,
			ContentType:  "text/plain",
			Body:         body,
		})
	failOnError(err, "Failed to publish a message")
}

type Consumer Queue

func NewConsumer(uri string, queueName string) Consumer {
	conn, err := amqp.Dial(uri)
	failOnError(err, "Failed to connect to RabbitMQ")

	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")

	err = ch.Qos(
		1,     // prefetch count
		0,     // prefetch size
		false, // global
	)
	failOnError(err, "Failed to set QoS")

	q := NewQueue(*ch, queueName, *conn)

	return Consumer(*q)
}

func (c *Consumer) Consume(fn func(amqp.Delivery)) {
	msgs, err := c.Channel.Consume(
		c.Queue.Name, // queue
		"",           // consumer
		false,        // auto-ack
		false,        // exclusive
		false,        // no-local
		false,        // no-wait
		nil,          // args
	)

	failOnError(err, "Failed to register a consumer")

	for d := range msgs {
		fn(d)
	}
}
