package common

import "time"
import "qq/models"

type ProbeRequestMessage struct {
	CheckId       string `json:"cid"`
	CheckInterval int    `json:"cint"`
	Address       string `json:"addr"`
	Timeout       int    `json:"tm"`
	Type          int    `json:"t"`
	ResponseText  string `json:"rt,omitempty"`
}

type ProbeResponseMessage struct {
	CheckId       string    `json:"cid"`
	CheckInterval int       `json:"cint"`
	Duration      int64     `json:"dur"`
	Status        int       `json:"st,omitempty"`
	Timeout       bool      `json:"tm"`
	Error         bool      `json:"err"`
	Timestamp     time.Time `json:"ts"`
	Valid         bool      `json:"v"`
	Type          int       `json:"t"`
}

func NewProbeRequestMessage(c *models.Check) *ProbeRequestMessage {
	prm := new(ProbeRequestMessage)
	prm.CheckId = c.Id.Hex()
	prm.Address = c.Address.String()
	prm.CheckInterval = c.CheckInterval
	prm.Type = c.Type
	prm.Timeout = c.TimeoutMs

	switch c.Type {
	case models.GetCheckTypeByString(models.CHECK_TYPE_HTTP_CONTAINS):
		prm.ResponseText = c.ResponseText
	}

	return prm
}

func NewProbeResponseMessage(prm *ProbeRequestMessage, duration time.Duration, timeout bool) ProbeResponseMessage {
	pres := ProbeResponseMessage{}
	pres.CheckId = prm.CheckId
	pres.Duration = duration.Nanoseconds() / 1000 / 1000
	pres.CheckInterval = prm.CheckInterval
	pres.Type = prm.Type
	pres.Timestamp = time.Now()

	return pres
}

func (prm *ProbeResponseMessage) ToProbe() models.Probe {

	probe := models.Probe{
		Status:    prm.Status,
		Duration:  prm.Duration,
		Timeout:   prm.Timeout,
		Timestamp: prm.Timestamp,
		Valid:     prm.Valid,
	}

	return probe
}
