package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"net/url"
	"os"
	"qq/cli"
	"qq/common"
	"qq/models"
	"qq/ping"
	"strconv"
	"strings"
	"time"

	"github.com/streadway/amqp"
)

var activeRequests int

func main() {

	consQueueName := common.QUEUE_NAME_PROBE_REQUEST + "_" + strconv.Itoa(*cli.OptionProbeReqWorkerQueue)
	prodQueueName := common.QUEUE_NAME_PROBE_RESPONSE

	cons := common.NewConsumer(*cli.OptionRabbitUri, consQueueName)
	prod := common.NewProducer(*cli.OptionRabbitUri, prodQueueName)

	forever := make(chan bool)

	for i := 0; i < *cli.OptionProbeReqWorkerNum; i++ {
		go setWorkerFn(cons, prod)
	}

	log.Printf(" [*] Waiting for messages. To exit press CTRL+C")

	common.OnSignalHandler(os.Interrupt, func() {

		log.Println("Closing worker...")

		cons.Channel.Cancel("", false)
		cons.Channel.Close()

		for activeRequests > 0 {
			time.Sleep(time.Second * 1)
		}

		log.Println("All channels closed...")

		close(forever)
	})

	<-forever
}

func setWorkerFn(cons common.Consumer, prod common.Producer) {
	cons.Consume(func(d amqp.Delivery) {
		pr := common.ProbeRequestMessage{}
		err := json.Unmarshal(d.Body, &pr)

		if err != nil {
			return
		}

		var prm common.ProbeResponseMessage

		activeRequests++
		switch pr.Type {
		case models.GetCheckTypeByString(models.CHECK_TYPE_PING):
			start := time.Now()
			up, err := ping.Ping(pr.Address, int64(pr.Timeout))
			prm = common.NewProbeResponseMessage(&pr, time.Since(start), up == false && err != nil)
		case models.GetCheckTypeByString(models.CHECK_TYPE_HTTP_CONTAINS):
			duration, wasTimeout, resp, err := makeRequest(pr)
			prm = common.NewProbeResponseMessage(&pr, duration, wasTimeout)
			prm.Valid = !wasTimeout && responseHasString(resp, pr.ResponseText)
			if err != nil {
				prm.Error = true
			}
		case models.GetCheckTypeByString(models.CHECK_TYPE_HTTP_STATUS):
			duration, wasTimeout, resp, err := makeRequest(pr)
			prm = common.NewProbeResponseMessage(&pr, duration, wasTimeout)
			if err == nil {
				prm.Status = resp.StatusCode
			}
			if err != nil {
				prm.Error = true
			}
		}
		activeRequests--

		prmJson, err := json.Marshal(prm)

		log.Println(string(prmJson))

		if err != nil {
			return
		}

		prod.Send(prmJson)

		d.Ack(false)
	})
}

func responseHasError(err interface{}) {

}

func responseHasString(resp *http.Response, find string) bool {
	defer resp.Body.Close()
	var bodyString string

	if resp.StatusCode == 200 {
		bodyBytes, _ := ioutil.ReadAll(resp.Body)
		bodyString = string(bodyBytes)
	}

	return strings.Contains(bodyString, find)
}

func makeRequest(pr common.ProbeRequestMessage) (duration time.Duration, wasTimeout bool, resp *http.Response, err error) {

	wasTimeout = false

	log.Printf("Checking %s", pr.Address)

	timeout := time.Duration(time.Duration(pr.Timeout) * time.Millisecond)
	client := http.Client{
		Timeout: timeout,
	}

	req, err := http.NewRequest("GET", pr.Address, nil)
	if err != nil {
		log.Fatalln(err)
	}

	req.Header.Set("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.1 Safari/537.36")

	start := time.Now()
	resp, err = client.Do(req)
	duration = time.Since(start)

	switch err := err.(type) {
	case *url.Error:
		if err, ok := err.Err.(net.Error); ok && err.Timeout() {
			wasTimeout = true
			fmt.Println("ERROR 1")
		}
	case net.Error:
		if err.Timeout() {
			wasTimeout = true
			fmt.Println("ERROR 2")
		}
	}

	if err != nil && strings.Contains(err.Error(), "use of closed network connection") {
		wasTimeout = true
		fmt.Println("ERROR 3")
	}
	return
}
