package tests

import (
	"encoding/json"
	"time"

	"gopkg.in/mgo.v2/bson"

	"github.com/revel/revel"
	"github.com/revel/revel/testing"
)

import "qq/models"
import "net/url"

type MainTest struct {
	testing.TestSuite
}

var tt revel.Session

func (t *MainTest) Test0() {
	models.DbInstance.Database.DropDatabase()
}

func (t *MainTest) Test01Register() {
	t.PostForm("/api/register", url.Values{"email": {"foo@foo.pl"}, "password": {"123456"}, "plan": {"3"}})
	t.AssertOk()
	t.AssertContentType("application/json; charset=utf-8")
	t.AssertContains("true")
}

func (t *MainTest) Test02Login() {
	t.PostForm("/api/login", url.Values{"email": {"foo@foo.pl"}, "password": {"123456"}})
	t.AssertOk()
	t.AssertContentType("application/json; charset=utf-8")
	t.AssertContains("true")
}

func (t *MainTest) Test03User() {
	t.Get("/api/user")
	t.AssertOk()
	t.AssertContentType("application/json; charset=utf-8")
	t.AssertNotContains("false")
	t.AssertContains("foo@foo.pl")
}

func (t *MainTest) Test04Logout() {
	t.Get("/api/logout")
	t.AssertOk()
	t.AssertContentType("application/json; charset=utf-8")
	t.AssertContains("true")
}

func (t *MainTest) Test05UserAfterLogout() {
	t.Get("/api/user")
	t.AssertOk()
	t.AssertContentType("application/json; charset=utf-8")
	t.AssertContains("false")
}

func (t *MainTest) Test06Login() {
	t.PostForm("/api/login", url.Values{"email": {"foo@foo.pl"}, "password": {"123456"}})
	t.AssertOk()
	t.AssertContentType("application/json; charset=utf-8")
	t.AssertContains("true")
}

var err error
var chid string

func (t *MainTest) Test10CheckPing() {
	t.PostForm("/api/check/ping/new", url.Values{
		"name": {"foo"}, "address": {"http://foo.com"},
		"timeoutMs": {"2000"}, "chInterval": {"900"},
	})
	t.AssertOk()
	t.AssertContentType("application/json; charset=utf-8")

	json.Unmarshal(t.ResponseBody, &chid)
	t.Assert(len(chid) == 24)

	t.Get("/api/check/" + chid)
	t.AssertOk()
	t.AssertContentType("application/json; charset=utf-8")
	var ch models.Check
	err = json.Unmarshal(t.ResponseBody, &ch)
	t.Assert(err == nil && ch.Id.Hex() == chid)

	t.Get("/api/checks")
	t.AssertOk()
	t.AssertContentType("application/json; charset=utf-8")
	var chArr []models.Check
	err = json.Unmarshal(t.ResponseBody, &chArr)
	t.Assert(err == nil && len(chArr) == 1)
}

func (t *MainTest) Test11CheckHttpContains() {
	t.PostForm("/api/check/http_contains/new", url.Values{
		"name": {"foo"}, "address": {"http://foo.com"},
		"timeoutMs": {"2000"}, "chInterval": {"900"},
		"contains": {"foo_bar"},
	})
	t.AssertOk()
	t.AssertContentType("application/json; charset=utf-8")

	json.Unmarshal(t.ResponseBody, &chid)
	t.Assert(len(chid) == 24)

	t.Get("/api/check/" + chid)
	t.AssertOk()
	t.AssertContentType("application/json; charset=utf-8")
	var ch models.Check
	err = json.Unmarshal(t.ResponseBody, &ch)
	t.Assert(err == nil && ch.Id.Hex() == chid)

	t.Get("/api/checks")
	t.AssertOk()
	t.AssertContentType("application/json; charset=utf-8")
	var chArr []models.Check
	err = json.Unmarshal(t.ResponseBody, &chArr)
	t.Assert(err == nil && len(chArr) == 2)
}

func (t *MainTest) Test12CheckHttpStatus() {
	t.PostForm("/api/check/http_status/new", url.Values{
		"name": {"foo"}, "address": {"http://foo.com"},
		"timeoutMs": {"2000"}, "chInterval": {"900"},
		"status": {"200"},
	})
	t.AssertOk()
	t.AssertContentType("application/json; charset=utf-8")

	json.Unmarshal(t.ResponseBody, &chid)
	t.Assert(len(chid) == 24)

	t.Get("/api/check/" + chid)
	t.AssertOk()
	t.AssertContentType("application/json; charset=utf-8")
	var ch models.Check
	err = json.Unmarshal(t.ResponseBody, &ch)
	t.Assert(err == nil && ch.Id.Hex() == chid)

	t.Get("/api/checks")
	t.AssertOk()
	t.AssertContentType("application/json; charset=utf-8")
	var chArr []models.Check
	err = json.Unmarshal(t.ResponseBody, &chArr)
	t.Assert(err == nil && len(chArr) == 3)
}

func (t *MainTest) Test20Check() {
	probe1 := models.Probe{time.Now(), false, 200, 1000, true}
	probe2 := models.Probe{time.Now().Add(time.Duration(time.Second)), false, 200, 1000, true}
	probe3 := models.Probe{time.Now().Add(-time.Duration(time.Hour * 24)), false, 200, 1000, true}
	probe4 := models.Probe{time.Now().Add(-time.Duration(time.Hour * 48)), false, 200, 1000, true}
	models.AddProbeToGroup(bson.ObjectIdHex(chid), probe1)
	models.AddProbeToGroup(bson.ObjectIdHex(chid), probe2)
	models.AddProbeToGroup(bson.ObjectIdHex(chid), probe3)
	models.AddProbeToGroup(bson.ObjectIdHex(chid), probe4)

	t.Get("/api/check/" + chid + "/probes")
	t.AssertOk()
	t.AssertContentType("application/json; charset=utf-8")
	var pArr []models.Probe
	err = json.Unmarshal(t.ResponseBody, &pArr)
	t.Assert(err == nil && len(pArr) == 4)
}

func (t *MainTest) Before() {
	if tt != nil {
		t.TestSuite.Session = tt
	}
}
func (t *MainTest) After() {
	tt = t.TestSuite.Session
}
