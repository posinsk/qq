package controllers

import (
	"encoding/json"

	"github.com/revel/revel"
)
import "qq/models"

type Main struct {
	*revel.Controller
}

/**
 * @api {post} /api/login Login
 * @apiGroup Main
 *
 * @apiParam {String} mail Email użytkownika
 * @apiParam {String} password Hasło
 */
func (c Main) Login(email string, password string) revel.Result {
	c.Validation.Clear()

	if _, ok := c.Session["uid"]; ok {
		return c.RenderJson(true)
	}

	user := models.NewUser(email, password)
	user = *user.ValidateLogin(c.Validation)

	if c.Validation.HasErrors() {
		c.Validation.Keep()
		c.FlashParams()
		return c.RenderJson(c.Validation.Errors)
	} else {
		models.UpdateLastLogin(user.Id)
		c.Session["uid"] = user.Id.Hex()
		bytes, _ := json.Marshal(user)
		c.Session["user"] = string(bytes)
		return c.RenderJson(true)
	}
}

/**
 * @api {get} /api/logout Logout
 * @apiGroup Main
 */
func (c Main) Logout() revel.Result {
	for k := range c.Session {
		delete(c.Session, k)
	}
	return c.RenderJson(true)
}

/**
 * @api {post} /api/register Register
 * @apiGroup Main
 *
 * @apiParam {String} mail Email użytkownika
 * @apiParam {String} password Hasło
 * @apiParam {Int} plan Plan abonamentowy
 */
func (c Main) Register(email string, password string, plan int) revel.Result {
	c.Validation.Clear()

	user := models.NewUser(email, password)
	user.SetSubscriptionPlan(plan)
	user.ValidateRegister(c.Validation)

	if c.Validation.HasErrors() {
		c.Validation.Keep()
		c.FlashParams()
		return c.RenderJson(c.Validation.Errors)
	}

	user.Save()

	return c.RenderJson(true)
}
