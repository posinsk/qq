package controllers

import "github.com/revel/revel"

import "qq/models"
import "time"
import "encoding/json"
import "gopkg.in/mgo.v2/bson"

type App struct {
	Main
}

func (c App) checkUser() revel.Result {
	if _, ok := c.Session["uid"]; !ok {
		return c.RenderJson(false)
	}

	return nil
}

func (c App) getUser() models.User {
	u := models.User{}
	json.Unmarshal([]byte(c.Session["user"]), &u)

	return u
}

func (c App) getUid() bson.ObjectId {
	return bson.ObjectIdHex(c.Session["uid"])
}

/**
 * @api {get} /api/user User
 * @apiGroup App
 */
func (c App) User() revel.Result {
	u, _ := models.FindUserById(c.getUid())
	return c.RenderJson(u)
}

func (c App) Dashboard() revel.Result {
	return c.RenderJson("Dash")
}

/**
 * @api {get} /api/checks Check list
 * @apiGroup App
 */
func (c App) GetChecks() revel.Result {
	return c.RenderJson(models.FindAllChecks(c.getUid()))
}

/**
 * @api {get} /api/check/:id Check
 * @apiGroup App
 * @apiParam {String} id Id czujki
 */
func (c App) GetCheck(id string) revel.Result {
	ch, err := models.FindOneCheck(bson.ObjectIdHex(id), c.getUid())

	if err != nil {
		return c.RenderJson(false)
	}
	return c.RenderJson(ch)
}

/**
 * @api {post} /api/check/ping/new Add ping check
 * @apiGroup App
 * @apiParam {String} name Nazwa
 * @apiParam {String} address Url
 * @apiParam {Int} timeoutMs Timeout odpytania
 * @apiParam {Int} chInterval Co ile odpytywać (w sekundach)
 */
func (c App) AddCheckPing(name, address string, timeoutMs, chInterval int) revel.Result {
	c.Validation.Clear()

	ch := models.NewCheck(c.getUid(), name, address, models.CHECK_TYPE_PING, timeoutMs, chInterval)
	ch.ValidateNew(c.Validation, c.getUser())

	return c.saveCheckAndGetResponse(ch)
}

/**
 * @api {post} /api/check/http_status/new Add http status check
 * @apiGroup App
 * @apiParam {String} name Nazwa
 * @apiParam {String} address Url
 * @apiParam {Int} timeoutMs Timeout odpytania
 * @apiParam {Int} chInterval Co ile odpytywać (w sekundach)
 * @apiParam {Int} status Oczekiwany status HTTP
 */
func (c App) AddCheckHttpStatus(name, address string, timeoutMs, chInterval, status int) revel.Result {
	c.Validation.Clear()

	ch := models.NewCheck(c.getUid(), name, address, models.CHECK_TYPE_HTTP_STATUS, timeoutMs, chInterval)
	ch.ResponseStatus = status
	ch.ValidateNew(c.Validation, c.getUser())
	ch.ValidateNewHttpStatus(c.Validation)

	return c.saveCheckAndGetResponse(ch)
}

/**
 * @api {post} /api/check/http_contains/new Add http contains check
 * @apiGroup App
 * @apiParam {String} name Nazwa
 * @apiParam {String} address Url
 * @apiParam {String} contains Ciąg znaków którego szukać w odpowiedzi
 * @apiParam {Int} timeoutMs Timeout odpytania
 * @apiParam {Int} chInterval Co ile odpytywać (w sekundach)
 */
func (c App) AddCheckHttpContains(name, address, contains string, timeoutMs, chInterval int) revel.Result {
	c.Validation.Clear()

	ch := models.NewCheck(c.getUid(), name, address, models.CHECK_TYPE_HTTP_CONTAINS, timeoutMs, chInterval)
	ch.ResponseText = contains
	ch.ValidateNew(c.Validation, c.getUser())
	ch.ValidateNewHttpContains(c.Validation)

	return c.saveCheckAndGetResponse(ch)
}

func (c App) saveCheckAndGetResponse(ch *models.Check) revel.Result {
	if c.Validation.HasErrors() {
		c.Validation.Keep()
		c.FlashParams()
		return c.RenderJson(c.Validation.Errors)
	} else {
		ch.Save()
	}

	return c.RenderJson(ch.Id.Hex())
}

/**
 * @api {get} /api/check/:id/probes Probes for check
 * @apiGroup App
 * @apiParam {String} id Id checka
 * @apiParam {String} [from] Zakres od
 * @apiParam {String} [to] Zakres do
 */
func (c App) GetProbes(id string) revel.Result {
	chId := bson.ObjectIdHex(id)

	if chId.Valid() == false {
		return c.RenderJson(false)
	}

	_, err := models.FindOneCheck(chId, c.getUid())

	if err != nil {
		return c.RenderJson(false)
	}

	var from, to time.Time
	if len(c.Params.Get("from")) == 0 {
		from = time.Now().Add(-time.Duration(time.Hour * 24 * 7))
	}

	if len(c.Params.Get("to")) == 0 {
		to = time.Now()
	}

	return c.RenderJson(models.GetProbes(chId, from, to))
}
